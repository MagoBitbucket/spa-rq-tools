

function ImageRegistration (varargin)

%%------------------------------------------------------------------------
%%  IMAGEREGISTRATION
%
%   This function is based on imregconfig
%
%   Margarita Walliander        Date: 11/02/2019
%%------------------------------------------------------------------------


F = varargin{1};
M = varargin{2};
NameOut = varargin{3}; 
Iteration = varargin{4};
% 
F1 = imread(F); 
M1 = imread(M); 

fixed = rgb2gray(F1); 
moving = rgb2gray(M1); 

[optimizer,metric] = imregconfig('multimodal');

%% optimizer initial radious smaller ;)
optimizer.InitialRadius = optimizer.InitialRadius/3.5;

%% optimizer iterations from 100 to 300
optimizer.MaximumIterations = Iteration;

h = waitbar(0.2,'computing...','Name','Progress');


tformSimilarity = imregtform(moving,fixed,'similarity',optimizer,metric);
waitbar(0.8, h, 'computing...');
Rfixed = imref2d(size(fixed));

moving1 = M1(:,:,1); 
moving2 = M1(:,:,2); 
moving3 = M1(:,:,3); 

moving1Rigid = imwarp(moving1,tformSimilarity,'OutputView',Rfixed);
moving2Rigid = imwarp(moving2,tformSimilarity,'OutputView',Rfixed);
moving3Rigid = imwarp(moving3,tformSimilarity,'OutputView',Rfixed);

movingRES = cat(3, moving1Rigid, moving2Rigid, moving3Rigid); 
f = figure('Name','Registered Image'); 

imshow(movingRES);

imwrite(movingRES, NameOut)
close(f) 
delete(h) 

