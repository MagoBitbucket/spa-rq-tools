
function SetDefaultFolder(varargin)

%%------------------------------------------------------------------------
%%  SETDEFAULTFOLDER
%
%   This function ignores the content at varargin{1}
%   and assigns the line at varargom{2} as the new content of the file
%
%   fileInput : contains the address of the file to modify
%   folderText:  is a string with the address of a folder
%
%   Margarita Walliander        Date: 19/06/2014
%%------------------------------------------------------------------------


narginchk ( 2, 2);

fileInput   = varargin{1};
folderText  = varargin{2};

fid         = fopen(fileInput, 'w');    
   
fwrite(fid, folderText);
%fclose(fid);
fclose('all');


end