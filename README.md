# Spa-RQ-Tools repository

Description:
Spa-RQ is an open-source software that performs image registration and staining overlap quantification on microscopy images, 
in such way spatial phenotypes on stained serial tissue sections can be studied.

Spa-Q folder contains the code related to quantifications.
Spa-R folder contains the code related to image registration.
src folder contatins the common source code related to both tools.
resources folder contains the icons and splash images.

Software version: 1.0

Requirements:
The code is written in MATLAB especially in MATLAB version 2016a.

Project members:
Owner and MATLAB developer: Margarita Walliander <margarita.walliander@gmail.com> Postdoc at Institute for Molecular Medicine Finland (FIMM), University of Helsinki, devoted to develop computerized analysis, quantification and segmentation of digitized tissue samples.
Co-owner and Biology consultant: Jie Bao <jie.bao@helsinki.fi> PhD student in Biomedicine, University of Helsinki, works on lung cancer phenotyping both in vivo and in vitro.
Tech. consultant: Ferenc Kovacs <kovacs.ferenc@single-cell-technologies.com>

Organization:
Institute for Molecular Medicine Finland (FIMM), University of Helsinki




