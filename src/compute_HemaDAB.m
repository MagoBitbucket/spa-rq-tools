function [I_HE, I_EO] = compute_HemaDAB(varargin)

%%------------------------------------------------------------------------
%%  COTE_HEMADAB
%
%   Use of macura color decomposition with 'he2' option
%
%   Margarita Walliander        Date: 11/02/2019
%%------------------------------------------------------------------------


narginchk (1, 1);
ITile   = varargin{1};
    
 rim = color_deconvolution_macura_give(ITile, 'he2');
 
 
hemat   = double(rim(: , : , 1));  
rim1c   = imcomplement(hemat);
mini    = min(rim1c(:));
maxi    = max(rim1c(:));
I_HE    = mat2gray(rim1c, [mini maxi]);    

eosin   = double(rim(: , : , 2));      
rim1c   = imcomplement(eosin);
mini    = min(rim1c(:));
maxi    = max(rim1c(:));
I_EO    = mat2gray(rim1c, [mini maxi]);

end
