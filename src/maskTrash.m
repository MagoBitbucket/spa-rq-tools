function mask = maskTrash(Gray, threshold, trash)
%%-----------------------------------------------
% MASKTRASH
%  This function loads a grayscale image, 
%  a threshold to binarize and a trash(size) level
%  
%   output  :
%   mask    : binary mask at threshold without trash
%
%   Margarita Walliander        Date: 06/08/2012
%%-----------------------------------------------

mask = false(size(Gray));
mask( Gray > threshold) = true;
%mask = imfill(mask,'holes');
mask = bwareaopen(mask,trash); %this depends on how big the trash is
end
