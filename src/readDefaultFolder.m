
function FolderInUse = readDefaultFolder(varargin)

%%------------------------------------------------------------------------
%%  READDEFAULTFOLDER
%
%   This function reads the content of a txt file
%   and returns it
%
%   fileInput : contains the address of the txt file which contains as
%   unique line a string with the address of folder
%
%   Margarita Walliander        Date: 19/06/2014
%%------------------------------------------------------------------------

narginchk ( 2, 2);

fileInput   = varargin{1};
FolderInput = varargin{2};

fid         = fopen(fileInput,'r+');    

if fid == -1    
    fid1         = fopen(fileInput,'w+');    
    FolderInUse = FolderInput;  
    fwrite (fid1, FolderInUse);      
    fclose('all');
else    
    FolderInUse       = fgetl(fid);  
    fclose('all');
    
    if ~exist(FolderInUse,'dir')
        fid1         = fopen(fileInput,'w+');    
        FolderInUse = FolderInput;  
        fwrite (fid1, FolderInUse);      
        fclose('all');
    end            
end

end