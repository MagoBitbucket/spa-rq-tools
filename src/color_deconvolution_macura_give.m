function [rim, map1, map2, map3, OD] = color_deconvolution_macura_give(varargin)
% [rim,map1,map2,map3] = color_deconvolution_macura(im)
%   Determines color deconvolution for the image IM using default color vectors, i.e. H&E
%
%   The function returns color (or stain) channels in m*n*3 matrix RIM;
%   RIM(:,:,1) = stain_1
%   RIM(:,:,2) = stain_2
%   RIM(:,:,3) = stain_3, and corresponding colormaps MAP1,MAP2&MAP3
%
% [rim,map1,map2,map3] = color_deconvolution_macura(im, mode)
%
%   Use predetermined colorvectors with option MODE;
%   'he','he2','h  dab','fastred fastblue dab','methyl green dab',
%   'h aec','axan-mallory','alcian blue & h','h pas','rgb','cmy'
%
% [rim,map1,map2,map3,OD] = color_deconvolution_macura(im, 'freehand')
% [rim,map1,map2,map3,OD] = color_deconvolution_macura(im, 'freehand',N)
%
%   Define color vectors manually by selecting regions from the input image. 
%   Numer of regions is defined with N, 0 < N > 4.
%
% [rim,map1,map2,map3] = color_deconvolution_macura(im,OD)
%
%   Input our own color vectors in OD.
%
% EXAMPLE:
%   
%   [rim,map1,map2,map3] = color_deconvolution_macura(im,'h dab');
%   figure,imshow(rim(:,:,1)),colormap(map1)
%   figure,imshow(im)
%   figure,imshow(rim(:,:,2)),colormap(map2)
%
%   Convert stain channel and colormap to true color image:
%   im_dab = ind2rgb( rim(:,:,2),map2 );
%
% The implementation is based on: 
% "Quantification of histochemical staining by color deconvolution" 
% by AC Ruifrok, DA Johnston - Analytical and quantitative cytology - 2001
%
%

im = varargin{1};

% calculate optical density image
OD_im = RGB2OD(im);


% define DAB & H OD-vectors
OD{1} = [0.00 0.00 0.00]; % [ MODx[0] MODy[0] MODz[0] ]
OD{2} = [0.00 0.00 0.00]; % [ MODx[1] MODy[1] MODz[1] ]
OD{3} = [0.00 0.00 0.00]; % [ MODx[2] MODy[2] MODz[2] ]

try
    
    if nargin > 1 && ischar(varargin{2})
        
        switch lower(varargin{2})
            
            case {'select','freehand'}
                
                if nargin > 2 && isnumeric(varargin{3}) && varargin{3} > 4
                    s_num_vectors = varargin{3};
                else
                    s_num_vectors = 3;
                end
                
                fig1 = figure;
                figure(fig1),imshow(im)
                
                for i=1:s_num_vectors
                    
                    rect{i} = uint16(getrect(gcf));
                    OD{i}   = OD_im( rect{i}(2):rect{i}(2)+rect{i}(4), rect{i}(1):rect{i}(1)+rect{i}(3),: );
                    OD{i}   = mean(mean(OD{i} ,1),2);
                    OD{i}   = reshape(OD{i},1,3);
                    
                    
                end
                %varargout = OD;
                
                
                close(fig1)
                
            case {'he','h&e'}
                
                OD{1} = [.644 .717 .267];
                OD{2} = [.093 .954 .283];
                
            case {'he2','h&e2'}
                
                OD{1} = [.650 .704 .286];
                OD{2} = [.072 .954 .283];
                
            case {'h dab','hdab'}
                
                OD{1} = [.650 .704 .286];
                OD{2} = [.268 .570 .776];
                
            case {'fastred fastblue dab'}
                
                OD{1} = [.214 .852 .478];
                OD{2} = [.749 .606 .267];
                OD{3} = [.268 .570 .776];
                
            case {'methyl green dab'}
                
                OD{1} = [.980 .144 .133];
                OD{2} = [.268 .570 .776];
                
            case {'he dab','h&e dab'}
                
                OD{1} = [.650 .704 .286];
                OD{2} = [.072 .954 .283];
                OD{3} = [.268 .570 .776];
                
            case {'h aec'}
                
                OD{1} = [.650 .704 .286];
                OD{2} = [.274 .680 .680];
                
            case {'azan-mallory'}
                
                OD{1} = [.853 .509 .113];
                OD{2} = [.071 .978 .198];
                
            case {'alcian blue & h'}
                
                OD{1} = [.875 .458 .158];
                OD{2} = [.553 .754 .354];
                
            case {'h pas'}
                
                OD{1} = [.650 .704 .286];
                OD{2} = [.175 .972 .155];
                
            case {'rgb'}
                
                OD{1} = [0 1 1];
                OD{2} = [1 0 1];
                OD{3} = [1 1 0];
                
            case {'cmy'}
                
                OD{1} = [1 0 0];
                OD{2} = [0 1 0];
                OD{3} = [0 0 1];
                
            otherwise
                
                disp('Cannot do that! What about HE?')                
                
        end
        
    else
        
        if nargin > 1 && isnumeric(varargin{1})
            
            OD(1) = varargin{2}(1);
            OD(2) = varargin{2}(3);
            OD(3) = varargin{2}(2);
            
        else
            
            % use he as default
            OD{1} = [.644 .717 .267];
            OD{2} = [.093 .954 .283];
            
        end
        
    end
    
catch err
    
    disp(err.message)
    
end


% Define the color deconvolution matrix D, M^-1
M = [OD{1};OD{2};OD{3}];

M(isinf(M)) = 1;

for i=1:3
    
    % normalize vectors
    len = sqrt(sum(M(i,:) .* M(i,:)));
    if len ~= 0
        M(i,:) = M(i,:) ./ repmat(len,1,3);
    end
    
end

% 2nd color unspecific
if sum(abs(M(2,:))) == 0
    M(2,1) = M(1,3);
    M(2,2) = M(1,1);
    M(2,3) = M(1,2);
end

% 3nd color unspecific
if sum(abs(M(3,:))) == 0
    for i=1:3
        if (M(1,i)*M(1,i) + M(2,i)*M(2,i)) > 1
            M(3,i) = 0;
        else
            M(3,i) = sqrt( 1 - M(1,i)*M(1,i) - M(2,i)*M(2,i) );
        end
    end
end

leng = sqrt( sum(M(3,:).*M(3,:)) );
M(3,:) = M(3,:) ./ repmat(leng,1,3);

D = inv(M);

% apply transformation
[m n p] = size(im);
rim = reshape(OD_im,m*n,p);
rim = exp( -rim*D );
rim = reshape(rim,m,n,p);
rim = uint8(rim .* 255);

% make images + colormaps
value = (255 - [0:255]') ./ 255;

map1  = repmat( OD{1}(1:3),256,1);
map1  = exp( -map1 .* repmat(value,1,3));

map2  = repmat( OD{2}(1:3),256,1);
map2  = exp( -map2 .* repmat(value,1,3));

map3  = repmat( OD{3}(1:3),256,1);
map3  = exp( -map3 .* repmat(value,1,3));

cmap = [map1;map2; map3];
% colormap(cmap)

%  figure
%  subplot(131),imshow( rim(:,:,1) )
%  subplot(132),imshow( rim(:,:,2) )
% %  subplot(133),imshow( rim(:,:,3) )
% figure, imshow( rim(:,:,1) ),colormap(map1)
% figure, imshow( rim(:,:,2) ),colormap(map2)
% figure, imshow( rim(:,:,3) ),colormap(map3)
% 

end


function OD_im = RGB2OD(im)

im = im2double(im);
OD_im = -log(im);

end