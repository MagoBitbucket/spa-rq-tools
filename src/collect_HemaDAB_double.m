function  [hBinary,...
    hmaskHem,...
    hmaskDAB,...
    hAreaBin,...
    hAreaDAB,...
    hPercentDABonBin,...
    hAreaTotal,...
    hBinaryref,...
    hmaskHemref,...
    hmaskDABref,...
    hAreaBinref,...
    hAreaDABref,...
    hPercentDABonBinref,...
    hmaskinter,...
    hAreaInter,...
    hReserveMemflag, ...
    hmaskBoth, ...
    hmaskBothRef, ...
    hTotalTissue] = collect_HemaDAB_double(varargin)

%%------------------------------------------------------------------------
%%  COLLECT_HEMADAB_DOUBLE
%
%   This function takes care of all the quantification problems.
%   There is a flag that activates the saving mode and this function
%   generates the output (tif and excel files)
%
%   Margarita Walliander        Date: 11/02/2019
%%------------------------------------------------------------------------

%%  get input
narginchk ( 1 , 1 );
handles = varargin{1};
TotalNumROIs =  length(handles.listNumberOfROIs.String);
hReserveMemflag = false;
if ~handles.ReserveMemflag
    
    
    hBinary  =   cell(TotalNumROIs,1);
    hmaskHem =   cell(TotalNumROIs,1);
    hmaskDAB =  cell(TotalNumROIs,1);
    
    hAreaDAB =  cell(TotalNumROIs,1);
    
    hAreaTotal   = cell(TotalNumROIs,1);
    hBinaryref   =  cell(TotalNumROIs,1);
    hmaskHemref  =  cell(TotalNumROIs,1);
    hmaskDABref  =  cell(TotalNumROIs,1);
    
    hAreaDABref  =  cell(TotalNumROIs,1);
    
    hmaskinter =  cell(TotalNumROIs,1);
    hAreaInter =  cell(TotalNumROIs,1);
    hReserveMemflag = true;
    hmaskBoth  =  cell(TotalNumROIs,1);
    hmaskBothRef  =  cell(TotalNumROIs,1);
    hTotalTissue =  cell(TotalNumROIs,1);
    
end

scaleX = 1;
pixelSize = str2double(get(handles.editPixelNum,'String'));


% Registered
Image           = handles.f;
hemat = handles.I_HE;
eosin = handles.I_EO;

% Reference
Imageref   = handles.fref;
hematref = handles.Iref_HE;
eosinref = handles.Iref_EO;

% Registered
HemoThreshold1 = handles.hema1_in;
EosinThreshold1 = handles.DAB1_in;

% Reference
HemoThreshold2 = handles.hema2_in;
EosinThreshold2 = handles.DAB2_in;

Trash           = 0;

%% ----------- CALCULATING REGIONS FOR DEFINED THRESHOLDS -------------
kk = get(handles.listNumberOfROIs, 'Value');
handles.flagDone{kk} = 1;

% BINARY

hTotalTissue{kk} = bwareaopen(imfill(~im2bw(Image, 254/255), 'holes'),100);
Binary = ~im2bw(Image, (255-handles.binary1)/255);
Binaryref =~im2bw(Imageref, (255-handles.binary2)/255);


% HEMATOXYLIN

maskHema    = maskTrash(hemat, HemoThreshold1/255, Trash);
maskHema    = (maskHema & Binary);  %This is only Hematoxyline

maskHemaref = maskTrash(hematref, HemoThreshold2/255, Trash);
maskHemaref = (maskHemaref & Binaryref);

% DAB
maskDAB     = maskTrash(eosin, EosinThreshold1/255, Trash);
maskDAB     =  (maskDAB & Binary); %This is only DAB

maskDABref  = maskTrash(eosinref, EosinThreshold2/255, Trash);
maskDABref  = (maskDABref & Binaryref);

%Ensure that DAB area is subset  of He area
maskHemaDAB = maskDAB|maskHema; %Positive and Negative Tissue  "Whole stained Tissue"
outHe       = imoverlay(Image, maskHemaDAB, [63/255 72/255 204/255]);
hmaskBoth{kk}       = imoverlay(outHe, maskDAB, [255/255 242/255 0]);
maskHemaDABref      = maskDABref|maskHemaref;

outHeref            = imoverlay(Imageref, maskHemaDABref, [63/255 72/255 204/255]);
hmaskBothRef{kk}    = imoverlay(outHeref, maskDABref, [255/255 242/255 0]);


%% Looking for the mean intensity of the positive  registered

hBinary{kk}  = Binary;
hmaskHem{kk} = maskHema;
hmaskDAB{kk} = maskDAB;

hAreaBin{kk}        = sum(Binary(:))*(pixelSize*pixelSize)/(scaleX*scaleX);
hAreaHemaDAB{kk}    = sum(maskHemaDAB(:))*(pixelSize*pixelSize)/(scaleX*scaleX);
hAreaDAB{kk}        = sum(maskDAB(:))*(pixelSize*pixelSize)/(scaleX*scaleX);

hPercentDABonHemaDAB{kk}    = 100*hAreaDAB{kk}/hAreaHemaDAB{kk};
hPercentDABonBin{kk}        = 100*hAreaDAB{kk}/hAreaBin{kk};
hAreaTotal{kk}    = sum(hTotalTissue{kk}(:))*(pixelSize*pixelSize)/(scaleX*scaleX);

%% Looking for the mean intensity of the positive area reference

hBinaryref{kk}  = Binaryref;
hmaskHemref{kk} = maskHemaref;
hmaskDABref{kk} = maskDABref;

hAreaBinref{kk}     = sum(Binaryref(:))*(pixelSize*pixelSize)/(scaleX*scaleX);
hAreaHemaDABref{kk} = sum(maskHemaDABref(:))*(pixelSize*pixelSize)/(scaleX*scaleX);
hAreaDABref{kk}     = sum(maskDABref(:))*(pixelSize*pixelSize)/(scaleX*scaleX);

hPercentDABonHemaDABref{kk} = 100*hAreaDABref{kk}/hAreaHemaDABref{kk};
hPercentDABonBinref{kk}     = 100*hAreaDABref{kk}/hAreaBinref{kk};

hmaskinter{kk} = maskDABref&maskDAB;
hAreaInter{kk} = sum(hmaskinter{kk}(:))*(pixelSize*pixelSize)/(scaleX*scaleX);

hperOLoverDABref{kk}    = 100*hAreaInter{kk}/hAreaDABref{kk};
hperOLoverDAB{kk}       = 100*hAreaInter{kk}/hAreaDAB{kk};

hperOLoverTissueref{kk} = 100*hAreaInter{kk}/hAreaHemaDABref{kk};
hperOLoverTissue{kk} = 100*hAreaInter{kk}/hAreaHemaDAB{kk};

hperOLoverBinref{kk}    = 100*hAreaInter{kk}/hAreaBinref{kk};
hperOLoverBin{kk}       = 100*hAreaInter{kk}/hAreaBin{kk};




if handles.SavingMode == 1
    
    handles.SavingMode = 0;
    % ---------------------------------------------------------------------
    set(handles.edBusyBar,'BackgroundColor',[0 1 0],'String', ...
        ['saving ROI # ', num2str(kk), ' ...'] );
    pause(0.001);
    % ---------------------------------------------------------------------
    
    if ~exist(handles.DestinationFolder, 'dir')
        mkdir(handles.DestinationFolder);
    end
    
    ii = get(handles.choose_Registered, 'Value');
    jj = get(handles.choose_Reference, 'Value');
    
    
    
    Date        = {handles.TableGeneral.Date{1}; datestr(now,'mm-dd-yyyy')};
    SlideName   = {handles.TableGeneral.SlideName{1}; ''};
    ROINum      = {kk; ''};
    Type        = { 'Reference';  'Registered'};
    
    SlideNum    = { handles.SlideNumber(jj); handles.SlideNumber(ii)};
    Antibody    = { handles.TableGeneral.Antibody{jj}; handles.TableGeneral.Antibody{ii}};
    
    thresBIN    = {handles.slBin2.Value; handles.slBin1.Value};
    thresHE     = {handles.slHema2.Value; handles.slHema1.Value};
    thresDAB    = {handles.slDAB2.Value; handles.slDAB1.Value};
    
    
    areaBin     = { hAreaBinref{kk};hAreaBin{kk} };
    areaTissue  = {hAreaHemaDABref{kk} ; hAreaHemaDAB{kk} };
    areaDAB     = {hAreaDABref{kk} ; hAreaDAB{kk} };
    
    positivity  = {hPercentDABonBinref{kk}; hPercentDABonBin{kk} };
    positivity2  = {hPercentDABonHemaDABref{kk}; hPercentDABonHemaDAB{kk} };
    
    areaOL      = {hAreaInter{kk} ;hAreaInter{kk}};
    
    perOLinDAB       = {hperOLoverDABref{kk} ;hperOLoverDAB{kk}};
    perOLinTissue       = {hperOLoverTissueref{kk};hperOLoverTissue{kk}};
    perOLinBin       = {hperOLoverBinref{kk} ;hperOLoverBin{kk}};
    
    %% Saving PICs
    %WHOLE TISSUE AREA
    
    B = imoverlay(Imageref,~hTotalTissue{kk},'black');
    namefile = sprintf('TIS_%s_%s-%s_ROI%s.tif', handles.TableGeneral.SlideName{1},num2str(handles.TableGeneral.SlideNumber(jj)),handles.TableGeneral.Antibody{jj}, num2str(kk));
    imwrite( B,fullfile(handles.DestinationFolder, namefile));
    
    B = imoverlay(Image,~hTotalTissue{kk},'black');
    namefile = sprintf('TIS_%s_%s-%s_ROI%s.tif', handles.TableGeneral.SlideName{1},num2str(handles.TableGeneral.SlideNumber(ii)),handles.TableGeneral.Antibody{ii},  num2str(kk));
    imwrite( B,fullfile(handles.DestinationFolder, namefile));
    
    %BINARY Thresholded
    
    B = imoverlay(Imageref,~hBinaryref{kk},'black');
    namefile = sprintf('BIN_%s_%s-%s_ROI%s.tif', handles.TableGeneral.SlideName{1},num2str(handles.TableGeneral.SlideNumber(jj)),handles.TableGeneral.Antibody{jj}, num2str(kk));
    imwrite( B,fullfile(handles.DestinationFolder, namefile));
    
    B = imoverlay(Image,~hBinary{kk},'black');
    namefile = sprintf('BIN_%s_%s-%s_ROI%s.tif', handles.TableGeneral.SlideName{1},num2str(handles.TableGeneral.SlideNumber(ii)),handles.TableGeneral.Antibody{ii},  num2str(kk));
    imwrite( B,fullfile(handles.DestinationFolder, namefile));
    
    %DAB stained Tissue
    
    B = imoverlay(Imageref,~hmaskDABref{kk},'black');
    namefile = sprintf('DAB_%s_%s-%s_ROI%s.tif', handles.TableGeneral.SlideName{1},num2str(handles.TableGeneral.SlideNumber(jj)),handles.TableGeneral.Antibody{jj},  num2str(kk));
    imwrite( B,fullfile(handles.DestinationFolder, namefile));
    
    B = imoverlay(Image,~hmaskDAB{kk},'black');
    namefile = sprintf('DAB_%s_%s-%s_ROI%s.tif', handles.TableGeneral.SlideName{1},num2str(handles.TableGeneral.SlideNumber(ii)),handles.TableGeneral.Antibody{ii},  num2str(kk));
    imwrite( B,fullfile(handles.DestinationFolder, namefile));
    
    % Colocalization of both antibodies (co-registered)
    B = imoverlay(Imageref,(hmaskDABref{kk}),'yellow');
    C = imoverlay(B, hmaskDAB{kk}, [255/255 165/255 0]);
    D = imoverlay(C, hmaskinter{kk}, [255/255 0 0]);
    
    namefile = sprintf('OL_%s_%s-%s_%s-%s_ROI%s.tif',handles.TableGeneral.SlideName{1},num2str(handles.TableGeneral.SlideNumber(jj)),handles.TableGeneral.Antibody{jj},num2str(handles.TableGeneral.SlideNumber(ii)),handles.TableGeneral.Antibody{ii},  num2str(kk));
    imwrite( D,fullfile(handles.DestinationFolder, namefile));
    
    
    outTable = table(Date,SlideName,  ROINum, Type, SlideNum,...
        Antibody,thresBIN, thresHE,thresDAB,...
        areaBin,  areaDAB, positivity, ...
        areaOL, perOLinDAB, perOLinBin );
    
    nameTableOut = sprintf('STATS_%s_%s-%sR_ROI%s.xls', handles.TableGeneral.SlideName{1}, num2str(handles.TableGeneral.SlideNumber(jj)), num2str(handles.TableGeneral.SlideNumber(ii)), num2str(kk));
    writetable(outTable,fullfile(handles.DestinationFolder, nameTableOut));
    
    % ---------------------------------------------------------------------
    set(handles.edBusyBar,'BackgroundColor',[1 1 1],'String', ...
        ['SAVED    ROI # ', num2str(kk), ' ...'] );
    pause(0.001);
    % ---------------------------------------------------------------------
end



end