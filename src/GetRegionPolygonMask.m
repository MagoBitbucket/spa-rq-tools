function [ABW, Poly] = GetRegionPolygonMask(ImageHandler, lengthLimit)

%%-------------------------------------------------------------------------
%%  GETREGIONPOLYGONMASK
%   This function reads clicked key behaviour to generate annotations based
%   on a Image map located at ImageName
%
%   INPUT:
%           ImageName : image name (local PNG, TIF file)
%           ImageHandler: Table that contains three columns CX, CY, LABEL
%           excelCoordFile : is a .XLS file (it might not yet exist)
%           color_label: is a vector that contains each color tags per
%           label tytpe
%           Origx : X Coordinate of the UpperLeft corner of the crop
%           Origy : Y Coordinate of the UpperLeft corner of the crop
%           Indx : List of indexes of coordinates from original table file
%
%   OUTPUT:
%           Table: contains three columns uint16 CX, CY, LABEL
%
%   Margarita Walliander        Date: 13/10/2017 
%%-------------------------------------------------------------------------


%%  ---------------------------
j  = 1;

while true
    w = waitforbuttonpress;
    if ~w
        
        % mouse clicked!  (in the image)?
        MB = get(ImageHandler.figure1,'SelectionType');
        
        xold = 0;
        yold = 0;
        k = 0;
        switch MB
            case 'normal'
                while true
                    [xi, yi, but] = ginput(1);      % get a point
                    if ~isequal(but, 1)             % stop if not button 1
                        break
                    end
                    k = k + 1;
                    pts(k, 1) = xi;
                    pts(k, 2) = yi;
                    hold on
                    if xold
                        plot([xold xi], [yold yi], 'go-');  % draw as we go
                    else
                        plot(xi, yi, 'go');         % first point on its own
                    end
                    hold off
                    if isequal(k, lengthLimit) % max number of vertices for a polygon to be defined
                        break
                    end
                    xold = xi;
                    yold = yi;
                end
                Poly{j} = pts;
                h = impoly(gca, pts, 'Closed', true);
                BW{j} = h.createMask;
                ABW{j} = imfill(BW{j}, 'holes');
                
                j = j+1;
                delete(h)
            otherwise
                break;
                %we are not interested in other functions right now
        end
        
        
        %% -----   Image plot -----------------------------------------------------
        
    end
    
end

end