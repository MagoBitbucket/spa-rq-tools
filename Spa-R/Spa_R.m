function varargout = Spa_R(varargin)
% SPA_R MATLAB code for Spa_R.fig
%      SPA_R, by itself, creates a new SPA_R or raises the existing
%      singleton*.
%
%      H = SPA_R returns the handle to a new SPA_R or the handle to
%      the existing singleton*.
%
%      SPA_R('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SPA_R.M with the given input arguments.
%
%      SPA_R('Property','Value',...) creates a new SPA_R or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Spa_R_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Spa_R_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
% %      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Spa_R

% Last Modified by GUIDE v2.5 16-Jan-2019 22:10:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Spa_R_OpeningFcn, ...
                   'gui_OutputFcn',  @Spa_R_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Spa_R is made visible.
function Spa_R_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Spa_R (see VARARGIN)

% Choose default command line output for SelectAnnotationROIs_v1
handles.output          = hObject;

handles.pixelSize       = 0.23;
handles.LocalFolder     = cd;

handles.DefaultFolderTXT = strcat(handles.LocalFolder, '\folder.txt');  
handles.InputFolder  = readDefaultFolder(handles.DefaultFolderTXT, handles.LocalFolder); 

handles.DestFolderTXT = strcat(handles.LocalFolder, '\Destfolder.txt');  
handles.OutputFolder  = readDefaultFolder(handles.DestFolderTXT, handles.LocalFolder); 

initialize_gui(hObject, handles, false);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Spa_R wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Spa_R_OutputFcn(hObject, eventdata, handles) 

% Get default command line output from handles structure
varargout{1} = handles.output;



function initialize_gui(hObject, handles, false)
 if ~fopen(handles.DefaultFolderTXT,'a+') 
     disp('please create an empty file named folder.txt at root'); 
 end

WorkF = readDefaultFolder(handles.DefaultFolderTXT, handles.LocalFolder);
set(handles.edFolderWork,'String',WorkF);

DestinationF = readDefaultFolder(handles.DestFolderTXT , handles.LocalFolder);
set(handles.edFolderDestination,'String',DestinationF);

guidata(hObject, handles); 


% --- Executes on button press in pushbuttonReference.
function pushbuttonReference_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonReference (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    cdOld = cd();
    cd(handles.InputFolder); 

    [handles.TIFReferenceName, handles.pathNameRef] = uigetfile('*.tif','30/10/2017', 'MultiSelect', 'off');
    if isequal(handles.TIFReferenceName, 0)
        msgbox('Please select a TIF', 'Error');
        return
    end     
    
    cd(cdOld); 
    guidata(hObject,handles);

    
% --- Executes on button press in pushbuttonRegister.
function pushbuttonRegister_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonRegister (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 cdOld = cd();
    cd(handles.InputFolder); 
    
    handles.Single = get(handles.buttonSingle,'Value');
    if handles.Single  %single
        [handles.TIFSpotName, handles.pathNameSpots] = uigetfile('*.tif','30/10/2017', 'MultiSelect', 'off');
        if isequal(handles.TIFSpotName, 0)
            msgbox('Please select a TIF', 'Error');
            return
        end
    else  %multiple
        [handles.TIFSpotName, handles.pathNameSpots] = uigetfile('*.tif', 'Batch of images to register 26/06/2017', 'MultiSelect', 'on');
    % Read all the spots to be register (at least 2)
        if isequal(handles.TIFSpotName, 0) || ~iscell(handles.TIFSpotName)
            msgbox('Please select at least 2 tif files.', 'Error');
            return
        end
    end
cd(cdOld); 

guidata(hObject,handles);


% --- Executes on button press in pushbuttonRUN.
function pushbuttonRUN_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonRUN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% calculate the number of HE reference

set(handles.edBusyBar,'BackgroundColor',[1 1 0],'String', 'computing...' );
pause(0.001); 


k = findstr(handles.TIFReferenceName, '_'); 
l = findstr(handles.TIFReferenceName, '-'); %here the name of the molecule antibody starts
numberReferenceSpot = handles.TIFReferenceName(k(end)+1:l-1);
RootName = handles.TIFReferenceName(1:k(end));
Iterations = str2double(get(handles.edNumIterations,'String')); 
SpotNameReferenceI = sprintf('%s%s', handles.pathNameRef, handles.TIFReferenceName);


if handles.Single %single
     m = findstr(handles.TIFSpotName, '_');
     l = findstr(handles.TIFSpotName, '-'); 
    
     numberOfSpot    = handles.TIFSpotName(k(end)+1:l-1);
     SpotNameI       = sprintf('%s%s',handles.pathNameRef,handles.TIFSpotName);
     SpotOutputName  = sprintf('%s\\%s%s-%sR.tif', handles.OutputFolder, RootName, numberReferenceSpot, numberOfSpot);
     ImageRegistration(SpotNameReferenceI, SpotNameI, SpotOutputName, Iterations);  
else
    
    for i = 1: length(handles.TIFSpotName)
        m = findstr(handles.TIFSpotName{i}, '_');
        l = findstr(handles.TIFSpotName{i}, '-'); 
    
        numberOfSpot    = handles.TIFSpotName{i}(k(end)+1:l-1);
        SpotNameI       = sprintf('%s%s',handles.pathNameRef,handles.TIFSpotName{i});
        SpotOutputName  = sprintf('%s%s%s-%sR.tif', handles.pathNameRef, RootName, numberReferenceSpot, numberOfSpot);
        ImageRegistration(SpotNameReferenceI, SpotNameI, SpotOutputName, Iterations);
    end
end
    
set(handles.edBusyBar,'BackgroundColor',[241/255 247/255 242/255],'String', ' ' );
pause(0.001);

guidata(hObject,handles);


function editNumberOfImages_Callback(hObject, eventdata, handles)
% hObject    handle to editNumberOfImages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editNumberOfImages as text
%        str2double(get(hObject,'String')) returns contents of editNumberOfImages as a double


% --- Executes during object creation, after setting all properties.
function editNumberOfImages_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editNumberOfImages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edFolderWork_Callback(hObject, eventdata, handles)
% hObject    handle to edFolderWork (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edFolderWork as text
%        str2double(get(hObject,'String')) returns contents of edFolderWork as a double


% --- Executes during object creation, after setting all properties.
function edFolderWork_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edFolderWork (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edFolderDestination_Callback(hObject, eventdata, handles)
% hObject    handle to edFolderDestination (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edFolderDestination as text
%        str2double(get(hObject,'String')) returns contents of edFolderDestination as a double


% --- Executes during object creation, after setting all properties.
function edFolderDestination_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in btnSelectWork.
function btnSelectWork_Callback(hObject, eventdata, handles)

handles.output  = hObject;

if handles.InputFolder~=0
    cd (handles.InputFolder);
   
else
    cd(handles.LocalFolder);
end
FolderName      = uigetdir; 
handles.InputFolder = FolderName;
set(handles.edFolderWork,'String',handles.InputFolder);
cd (handles.LocalFolder);

guidata(hObject, handles); 



% --- Executes on button press in btnSelectDestination.
function btnSelectDestination_Callback(hObject, eventdata, handles)

handles.output  = hObject;

if handles.OutputFolder~=0
    cd (handles.OutputFolder);
else
    cd (handles.LocalFolder);
end

FolderName      = uigetdir; 

handles.OutputFolder = FolderName;
set(handles.edFolderDestination,'String',handles.OutputFolder);
cd (handles.LocalFolder);

guidata(hObject, handles); 


% --- Executes on button press in btnSetDefaultWork.
function btnSetDefaultWork_Callback(hObject, eventdata, handles)

handles.DefaultFolderTXT = strcat(handles.LocalFolder, '\folder.txt');  
SetDefaultFolder(handles.DefaultFolderTXT, handles.InputFolder);

guidata(hObject,handles);


% --- Executes on button press in btnSetDefaultDestination.
function btnSetDefaultDestination_Callback(hObject, eventdata, handles)

handles.DestFolderTXT = strcat(handles.LocalFolder,'\', 'Destfolder.txt');
SetDefaultFolder(handles.DestFolderTXT, handles.OutputFolder);

guidata(hObject,handles);



% --- Executes on button press in buttonSingle.
function buttonSingle_Callback(hObject, eventdata, handles)
% hObject    handle to buttonSingle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% handles.Single = get(hObject,'Value');
% disp('g');
% Hint: get(hObject,'Value') returns toggle state of buttonSingle



function edNumIterations_Callback(hObject, eventdata, handles)
% hObject    handle to edNumIterations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edNumIterations as text
%        str2double(get(hObject,'String')) returns contents of edNumIterations as a double


% --- Executes during object creation, after setting all properties.
function edNumIterations_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edNumIterations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edBusyBar_Callback(hObject, eventdata, handles)
% hObject    handle to edBusyBar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edBusyBar as text
%        str2double(get(hObject,'String')) returns contents of edBusyBar as a double


% --- Executes during object creation, after setting all properties.
function edBusyBar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edBusyBar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
