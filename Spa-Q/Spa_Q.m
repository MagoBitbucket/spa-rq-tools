function varargout = Spa_Q(varargin)
%SPA_Q M-file for Spa_Q.fig
%      SPA_Q, by itself, creates a new SPA_Q or raises the existing
%      singleton*.
%
%      H = SPA_Q returns the handle to a new SPA_Q or the handle to
%      the existing singleton*.
%
%      SPA_Q('Property','Value',...) creates a new SPA_Q using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to Spa_Q_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      SPA_Q('CALLBACK') and SPA_Q('CALLBACK',hObject,...) call the
%      local function named CALLBACK in SPA_Q.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Spa_Q
% 
% Last Modified by GUIDE v2.5 27-Jan-2019 01:08:59
% By Margarita Walliander 
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Spa_Q_OpeningFcn, ...
    'gui_OutputFcn',  @Spa_Q_OutputFcn, ...
    'gui_LayoutFcn',  [], ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Spa_Q is made visible.
function Spa_Q_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for Spa_Q
handles.output          = hObject;

handles.index           = 1;
handles.low_in          = 0;
handles.high_in         = 1;
handles.alpha           = 1;

handles.flagdeconvolution = false;  % Used to inform if detection was performed


handles.pixelSize       = 0.23; %For Jie images
handles.LocalFolder     = cd;
handles.maskBoth        = [];
handles.maskBothRef        = [];

handles.DefaultFolderTXT = strcat(handles.LocalFolder, '\folder.txt');
handles.WorkFolder  = readDefaultFolder(handles.DefaultFolderTXT, handles.LocalFolder);

handles.DestFolderTXT = strcat(handles.LocalFolder, '\Destfolder.txt');
handles.DestinationFolder  = readDefaultFolder(handles.DestFolderTXT, handles.LocalFolder);

initialize_gui(hObject, handles, false);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Spa_Q wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Spa_Q_OutputFcn(hObject, eventdata, handles)
varargout{1} = handles.output;


function initialize_gui(hObject, handles, false)
if ~fopen(handles.DefaultFolderTXT,'a+')
    disp('please create an empty file named folder.txt at root');
end

%set(handles.edFolderWork,'String', handles.WorkFolder);
WorkF = readDefaultFolder(handles.DefaultFolderTXT, handles.LocalFolder);
set(handles.edFolderWork,'String',WorkF);

DestinationF = readDefaultFolder(handles.DestFolderTXT , handles.LocalFolder);
set(handles.edFolderDestination,'String',DestinationF);


guidata(hObject, handles);
%


function InitSpace(hObject, eventdata, handles)

handles.low_in  = get(handles.slLowIn,'Value');
handles.high_in = get(handles.slHighIn,'Value');
handles.alpha   = get(handles.slAlpha,'Value');
handles.binary1  = get(handles.slBin1,'Value');%registered
handles.binary2  = get(handles.slBin2,'Value');%reference

% Cell detection depending on the grain Size
handles.f       = [];
handles.newf    = [];

handles.tlxc    = [];
handles.tlyc    = [];
handles.brxc    = [];
handles.bryc    = [];

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pushLoadFile.
function pushLoadFile_Callback(hObject, eventdata, handles)

cdOld = cd;
cd(handles.WorkFolder);
[excelReadName, pathName] = uigetfile({'*.xlsx';'*.csv';'*.xls'},'29/09/2017', 'MultiSelect', 'off');
cd(cdOld);

if isequal(excelReadName, 0)
    msgbox('Please select an excel file', 'Error');
    return
end

handles.pathName = pathName;

onlineFile = sprintf('%s%s', pathName, excelReadName);
handles.SavingMode = 0;
handles.TableGeneral = readtable(onlineFile);
handles.SlideNumber = handles.TableGeneral.SlideNumber;
handles.SlideName = handles.TableGeneral.SlideName;
handles.Antibody = handles.TableGeneral.Antibody;

handles.ResultsDirG = readDefaultFolder(handles.DestFolderTXT , handles.LocalFolder);
if exist(handles.ResultsDirG, 'dir') == 0
    mkdir(handles.ResultsDirG);
end

%Generate a list data files to select the registered that has been
%registered against a reference
for k = 1 : length(handles.SlideNumber)
    dropdownListStrings{k} =  handles.SlideNumber(k);
end
set(handles.choose_Registered,  'String', dropdownListStrings);

%Generate a list data files (same as before) used to choose the
%reference registered    -  choose the correct reference file
for k = 1 : length(handles.SlideNumber)
    dropdownListStrings{k} =  handles.SlideNumber(k);
end
set(handles.choose_Reference,  'String', dropdownListStrings);
set(handles.pushLoadFile, 'BackgroundColor', [1 215/255 174/255]);

guidata(hObject,handles);



% --- Executes on button press in pushlocc.
function pushAnnotation_Callback(hObject, eventdata, handles)


set(handles.pushAnnotation, 'BackgroundColor', [0.70 0.70 0.70]);

i = get(handles.choose_Registered, 'Value');
j = get(handles.choose_Reference, 'Value');

handles.lastROI = 100; 
handles.currentROI = 100;
handles.ReserveMemflag = false;

LocalScaleNum = 10;  %just for plotting
handles.LocalScaleNum = LocalScaleNum;
pathName = handles.pathName;
SlideName = handles.SlideName{1};
SlideNumber = handles.SlideNumber;
Antibody = handles.Antibody;

handles.ReferenceImage = sprintf('%s%s_%s-%s.tif', pathName, SlideName, num2str(SlideNumber(j)),  Antibody{j});
handles.RegisteredImage  = sprintf('%s%s_%s-%sR.tif', pathName, SlideName, num2str(SlideNumber(j)), num2str(SlideNumber(i)));

handles.ReferenceLabel = sprintf('%s_%s:   %s', SlideName, num2str(SlideNumber(j)),  Antibody{j});
handles.RegisteredLabel  = sprintf('%s_%s:   %s', SlideName, num2str(SlideNumber(i)), Antibody{i});


set(handles.lbFileReference,'String', handles.ReferenceLabel);
set(handles.lbFileRegistered,'String', handles.RegisteredLabel);


% REFERENCE
I = imread(handles.ReferenceImage);
Iresized = imresize(I, 1/LocalScaleNum);
im = im2single(Iresized(:,:,1:3));

handles.fref = im;
axes(handles.Reference);
imshow(handles.fref);

% REGISTERED
I = imread(handles.RegisteredImage);
Iresized = imresize(I, 1/LocalScaleNum);
im = im2single(Iresized(:,:,1:3));

handles.f = im;
axes(handles.Registered);

imshow(handles.f);


%% Here we start collecting the tumors
[handles.Masks, handles.Poly] = GetRegionPolygonMask(handles, 50);

NumberOfROIs =  numel(handles.Masks);
handles.listNumberofROIs  = NumberOfROIs;
dropdownListSubset = [];
for k = 1 : NumberOfROIs
    dropdownListSubset{k} =  k;
end
set(handles.listNumberOfROIs,  'String', dropdownListSubset);


set(handles.pushAnnotation, 'BackgroundColor', [1 215/255 174/255]);
guidata(hObject, handles);




% --- Executes on button press in pushbuttonDetailBorder.
function pushbuttonDetailBorder_Callback(hObject, eventdata, handles)

set(handles.pushbuttonDetailBorder, 'BackgroundColor', [0.70 0.70 0.70]);

i = get(handles.choose_Registered, 'Value');
j = get(handles.choose_Reference, 'Value');
k = get(handles.listNumberOfROIs, 'Value');


% ---------------------------------------------------------------------
set(handles.edBusyBar,'BackgroundColor',[0 1 0],'String', ['detailing polygon ROI # ', num2str(k), ' ...'] );
pause(0.001);
% ---------------------------------------------------------------------


handles.currentROI = k;

pathName    = handles.pathName;
SlideName   = handles.SlideName{1};
SlideNumber = handles.SlideNumber;
Antibody    = handles.Antibody;

delete('handles.MasksTumor')
delete('handles.PolyT')

set(handles.lbFileReference,'String', handles.ReferenceLabel );
set(handles.lbFileRegistered,'String', handles.RegisteredLabel );


I       = imread(handles.RegisteredImage);
Iref    = imread(handles.ReferenceImage);
[m, n, ~] = size(I);
[m2, n2, ~] = size(Iref);

if (m~=m2)|(n~=n2) %Check if the registration was done or done properly!
    disp('error in registration file');
    return;
end

Mask = handles.Masks{k};    es = strel ('disk', 10);
Mask = imdilate (Mask, es);
s    = regionprops(imresize(Mask,[m n]), 'BoundingBox');

Boxi = (s.BoundingBox);
handles.imi     = I(Boxi(2): Boxi(2)+Boxi(4)-1,Boxi(1): Boxi(1)+Boxi(3)-1,:);
handles.imiref  = Iref(Boxi(2): Boxi(2)+Boxi(4)-1,Boxi(1): Boxi(1)+Boxi(3)-1,:);


%% Here we start collecting accurated borders with polygon drawing tool
%Registered
axes(handles.Registered);    imshow(handles.imi); handles.f = handles.imi; axis off;
%Reference
axes(handles.Reference); imshow(handles.imiref); handles.fR = handles.imiref; axis off;

[handles.MasksTumor{k}, handles.PolyT] = GetRegionPolygonMask(handles, 300);

% ---------------------------------------------------------------------
set(handles.edBusyBar,'BackgroundColor',[0 1 0],'String', ['masking ROI # ', num2str(k), ' ...'] );
pause(0.001);
% ---------------------------------------------------------------------

Mask = handles.MasksTumor{k}{1,1};
es = strel ('disk', 5);
Mask = imerode(imopen (Mask, es), es);

%Registered
OUT = imoverlay(handles.imi, ~Mask, [1 1 1]);
handles.f = OUT;
axes(handles.Registered);
imshow(handles.f);

%Reference
OUTR = imoverlay(handles.imiref, ~Mask, [1 1 1]);
handles.fref = OUTR;
axes(handles.Reference);
imshow(handles.fref);
axis off;
guidata(hObject, handles);


% ---------------------------------------------------------------------
set(handles.edBusyBar, 'BackgroundColor',[1 1 0],'String', (['masking done for ROI ', num2str(k), ' !']) );
% ---------------------------------------------------------------------

%Measuring,... % if Saving Mode is active
handles = common_HemaDAB_call(hObject, eventdata, handles);

set(handles.pushbuttonDetailBorder, 'BackgroundColor', [1 215/255 174/255]);


guidata(hObject, handles);




function lbFileRegistered_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function lbFileRegistered_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slHema1_Callback(hObject, eventdata, handles)
common_HemaDAB_call(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function slHema1_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slDAB1_Callback(hObject, eventdata, handles)
common_HemaDAB_call(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function slDAB1_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function handles = common_HemaDAB_call(hObject, eventdata, handles)

% set(handles.pushLoadFile, 'BackgroundColor', [240/255 240/255 240/255]);
%REGISTERED
handles.hema1_in     = get(handles.slHema1,'Value');
handles.DAB1_in      = get(handles.slDAB1,'Value');

set(handles.edHemaValue1,'String',handles.hema1_in);
set(handles.edDABValue1,'String',handles.DAB1_in);

%REFERENCE
handles.hema2_in     = get(handles.slHema2,'Value');
handles.DAB2_in      = get(handles.slDAB2,'Value');

set(handles.edHemaValue2,'String',handles.hema2_in);
set(handles.edDABValue2,'String',handles.DAB2_in);

if ~strcmp(get(handles.btn_Fix_Binary1, 'String'), 'Fixed')
    handles         = common_Binary_call(hObject, eventdata, handles);
end
%----------------
if handles.lastROI ~= handles.currentROI%~handles.flagdeconvolution
    [handles.I_HE,handles.I_EO] = compute_HemaDAB(handles.f);
    [handles.Iref_HE,handles.Iref_EO] = compute_HemaDAB(handles.fref);
end

[handles.Binary,...
    handles.maskHem,... 
    handles.maskDAB,...
    handles.AreaBin,...
    handles.AreaDAB,...
    handles.PercentDABonBin,...
    handles.AreaTotal,...
    handles.Binaryref,...
    handles.maskHemref,...
    handles.maskDABref,... 
    handles.AreaBinref,...
    handles.AreaDABref,...
    handles.PercentDABonBinref,...
    handles.maskinter,...
    handles.AreaInter,...
    handles.ReserveMemflag,...
    handles.maskBoth, ...
    handles.maskBothRef, ...
    handles.TotalTissue] = collect_HemaDAB_double(handles);

kk = get(handles.listNumberOfROIs, 'Value');

%Registered
handles.newf            = imadjust(handles.maskBoth{kk}, [handles.low_in handles.high_in], [],  handles.alpha);
axes(handles.Registered);
imshow(handles.newf);
axis off;
set(handles.edArea, 'String', handles.AreaBin{kk});  %
set(handles.edAreaDAB, 'String', handles.AreaDAB{kk});
set(handles.edPercentDAB, 'String', handles.PercentDABonBin{kk});
%Reference
handles.newfref            = imadjust(handles.maskBothRef{kk}, [handles.low_in handles.high_in], [],  handles.alpha);
axes(handles.Reference);
imshow(handles.newfref);
set(handles.edArearef, 'String', handles.AreaBinref{kk});
set(handles.edAreaDABref, 'String', handles.AreaDABref{kk});
set(handles.edPercentDABref, 'String', handles.PercentDABonBinref{kk});
set(handles.edAreaInter, 'String', handles.AreaInter{kk});
set(handles.edAreaTotal, 'String', handles.AreaTotal{kk});


guidata(hObject,handles);


function handles = common_HemaDAB2_call(hObject, eventdata, handles)

% set(handles.pushLoadFile, 'BackgroundColor', [240/255 240/255 240/255]);

handles.hema2_in     = get(handles.slHema2,'Value');
handles.DAB2_in      = get(handles.slDAB2,'Value');

set(handles.edHemaValue2,'String',handles.hema2_in);
set(handles.edDABValue2,'String',handles.DAB2_in);

if ~strcmp(get(handles.btn_Fix_Binary2, 'String'), 'Fixed')
    handles         = common_Binary_call(hObject, eventdata, handles);
end

%----------------
if ~handles.flagdeconvolution
    [handles.I_HE,handles.I_EO] = compute_HemaDAB(handles.f);
    [handles.Iref_HE,handles.Iref_EO] = compute_HemaDAB(handles.fref);
    handles.flagdeconvolution  = true;
end

handles = collect_HemaDAB_double(handles);
%Registered
handles.newf            = imadjust(handles.maskBoth, [handles.low_in handles.high_in], [],  handles.alpha);
axes(handles.Registered);
imshow(handles.newf);
axis off;
set(handles.edTMI, 'String', handles.TMI);
set(handles.edFSA, 'String', handles.FSA);
set(handles.edACIA, 'String', handles.ACIA);
set(handles.edArea, 'String', handles.Area);

%Reference
handles.newfref            = imadjust(handles.maskBothRef, [handles.low_in handles.high_in], [],  handles.alpha);
axes(handles.Reference);
imshow(handles.newfref);
axis off;
set(handles.edTMIref, 'String', handles.TMIref);
set(handles.edFSAref, 'String', handles.FSAref);
set(handles.edACIAref, 'String', handles.ACIAref);

%Intersection
set(handles.edArearef, 'String', handles.Arearef);
%axes(handles.WholeImage);

guidata(hObject,handles);


function edHemaValue1_Callback(hObject, eventdata, handles)

a =  str2double(get(hObject,'String'));
set(handles.slHema1, 'Value', a);
common_HemaDAB_call(hObject, eventdata, handles);

guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edHemaValue1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edDABValue1_Callback(hObject, eventdata, handles)

a =  str2double(get(hObject,'String'));
set(handles.slDAB1, 'Value', a);
common_HemaDAB_call(hObject, eventdata, handles);

guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edDABValue1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edTMI_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function edTMI_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edFSA_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function edFSA_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edACIA_Callback(hObject, eventdata, handles)
% hObject    handle to edACIA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edACIA as text
%        str2double(get(hObject,'String')) returns contents of edACIA as a double


% --- Executes during object creation, after setting all properties.
function edACIA_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edACIA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slBin1_Callback(hObject, eventdata, handles)

common_Binary_call(hObject, eventdata, handles); %WORKS

% --- Executes during object creation, after setting all properties.
function slBin1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slBin1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edBinValue1_Callback(hObject, eventdata, handles)

a =  str2double(get(hObject,'String'));
set(handles.slBin1, 'Value', a);

handles = common_Binary_call(hObject, eventdata, handles);
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edBinValue1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function handles = common_Binary_call(hObject, eventdata, handles)

handles.binary1  = get(handles.slBin1,'Value');
handles.binary2  = get(handles.slBin2,'Value');

set(handles.edBinValue1,'String',handles.binary1);
set(handles.edBinValue2,'String',handles.binary2);

%Registered
handles.BW1      = ~im2bw(handles.f, (255-handles.binary1)/255);

out             = imoverlay(handles.f, ~handles.BW1,[0 0 0 ]);
handles.newf    = out;

axes(handles.Registered);
imshow(handles.newf);
set(handles.Registered,'Visible','on');
axis off
set(handles.Registered,'nextplot','replacechildren');


%Reference
handles.BW2      = ~im2bw(handles.fref, (255-handles.binary2)/255);

outR             = imoverlay(handles.fref, ~handles.BW2,[0 0 0 ]);
handles.newfref    = outR;

axes(handles.Reference);
imshow(handles.newfref);
set(handles.Reference,'Visible','on');
axis off
set(handles.Reference,'nextplot','replacechildren');

guidata(hObject,handles);

% --- Executes on button press in btn_Fix_Binary1.
function btn_Fix_Binary1_Callback(hObject, eventdata, handles)

set(handles.pushLoadFile, 'BackgroundColor', [240/255 240/255 240/255]);

if strcmp(get(handles.edBinValue1, 'Enable'), 'off')
    set(handles.edBinValue1,'Enable', 'on', 'BackgroundColor', [240/255 240/255 240/255]);
    set(handles.btn_Fix_Binary1, 'BackgroundColor', [240/255 240/255 240/255], 'String', 'Fix');
    
    set(handles.slBin1,'Enable', 'on');
else
    set(handles.edBinValue1,'Enable', 'off', 'BackgroundColor', [1 215/255 174/255]);
    set(handles.btn_Fix_Binary1, 'BackgroundColor', [1 215/255 174/255], 'String', 'Fixed');
    set(handles.slBin1,'Enable', 'off');
end

handles = common_Binary_call(hObject, eventdata, handles);
guidata(hObject,handles);




% --- Executes on button press in btnSelectWork.
function btnSelectWork_Callback(hObject, eventdata, handles)

handles.output  = hObject;

if handles.WorkFolder~=0
    cd (handles.WorkFolder);
   
else
    cd(handles.LocalFolder);
end
FolderName      = uigetdir; 
if FolderName==0
     FolderName  = handles.LocalFolder;
end

handles.WorkFolder = FolderName;
set(handles.edFolderWork,'String',handles.WorkFolder);
cd (handles.LocalFolder);

guidata(hObject, handles); 

% --- Executes on button press in btnSetDefaultWork.
function btnSetDefaultWork_Callback(hObject, eventdata, handles)

handles.DefaultFolderTXT = strcat(handles.LocalFolder, '\folder.txt');
SetDefaultFolder(handles.DefaultFolderTXT, handles.WorkFolder);

guidata(hObject,handles);



% --- Executes on button press in btnSetDefaultDestination.
function btnSetDefaultDestination_Callback(hObject, eventdata, handles)
%
handles.DestFolderTXT = strcat(handles.LocalFolder,'\', 'Destfolder.txt');
SetDefaultFolder(handles.DestFolderTXT, handles.DestinationFolder);

guidata(hObject,handles);




function edFolderDestination_Callback(hObject, eventdata, handles)


% --- Executes on button press in btnSelectDestination.
function btnSelectDestination_Callback(hObject, eventdata, handles)

handles.output  = hObject;

if handles.DestinationFolder~=0
    cd (handles.DestinationFolder);
else
    cd (handles.LocalFolder);
end

FolderName      = uigetdir; 
if FolderName==0
     FolderName  = handles.LocalFolder;
end

handles.DestinationFolder = FolderName;
set(handles.edFolderDestination,'String',handles.DestinationFolder);
cd (handles.LocalFolder);

guidata(hObject, handles); 

% --- Executes during object creation, after setting all properties.
function edFolderDestination_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in choose_Registered.
function choose_Registered_Callback(hObject, eventdata, handles)

handles.InputFileNumber     =  get(handles.choose_Registered, 'Value');
handles.flagGlobal  = false;
handles.flagdeconvolution  = false;
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function choose_Registered_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes on selection change in choose_Reference.
function choose_Reference_Callback(hObject, eventdata, handles)

handles.Reference = get(handles.choose_Reference, 'Value');



% --- Executes during object creation, after setting all properties.
function choose_Reference_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in btn_Fix_Hema1.
function btn_Fix_Hema1_Callback(hObject, eventdata, handles)

if strcmp(get(handles.edHemaValue1, 'Enable'), 'off')
    set(handles.edHemaValue1,'Enable', 'on', 'BackgroundColor', [240/255 240/255 240/255]);
    set(handles.btn_Fix_Hema1, 'BackgroundColor', [240/255 240/255 240/255], 'String', 'Fix');
    
    set(handles.slHema1,'Enable', 'on');
else
    set(handles.edHemaValue1,'Enable', 'off', 'BackgroundColor', [1 215/255 174/255]);
    set(handles.btn_Fix_Hema1, 'BackgroundColor', [1 215/255 174/255], 'String', 'Fixed');
    set(handles.slHema1,'Enable', 'off');
end


handles.hema1_in     = get(handles.slHema1,'Value');
set(handles.edHemaValue1,'String',handles.hema1_in);
handles = common_HemaDAB_call(hObject, eventdata, handles);
guidata(hObject,handles);


% --- Executes on button press in btn_Fix_DAB1.
function btn_Fix_DAB1_Callback(hObject, eventdata, handles)

if strcmp(get(handles.edDABValue1, 'Enable'), 'off')
    set(handles.edDABValue1,'Enable', 'on', 'BackgroundColor', [240/255 240/255 240/255]);
    set(handles.btn_Fix_DAB1, 'BackgroundColor', [240/255 240/255 240/255], 'String', 'Fix');
    
    set(handles.slDAB1,'Enable', 'on');
else
    set(handles.edDABValue1,'Enable', 'off', 'BackgroundColor', [1 215/255 174/255]);
    set(handles.btn_Fix_DAB1, 'BackgroundColor', [1 215/255 174/255], 'String', 'Fixed');
    set(handles.slDAB1,'Enable', 'off');
end

handles.DAB1_in      = get(handles.slDAB1,'Value');
set(handles.edDABValue1,'String',handles.DAB1_in);
handles = common_HemaDAB_call(hObject, eventdata, handles);
guidata(hObject,handles);



% --- Executes on slider movement.
function slLowIn_Callback(hObject, eventdata, handles)
common_imadjust_call(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function slLowIn_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slHighIn_Callback(hObject, eventdata, handles)
common_imadjust_call(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function slHighIn_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slAlpha_Callback(hObject, eventdata, handles)
common_imadjust_call(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function slAlpha_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function common_imadjust_call(hObject, eventdata, handles)

handles.low_in  = get(handles.slLowIn,'Value');
handles.high_in = get(handles.slHighIn,'Value');
handles.alpha   = get(handles.slAlpha,'Value');

if handles.flagBydetect
    handles.newf    = imadjust(handles.maskBoth, [handles.low_in handles.high_in], [], handles.alpha);
else
    handles.newf    = imadjust(handles.f, [handles.low_in handles.high_in], [],  handles.alpha);
end

imshow(handles.newf);
set(handles.Registered,'Visible','on');
axis off
set(handles.Registered,'nextplot','replacechildren');

handles.flagGlobal = true;
guidata(hObject,handles);



function edArea_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function edArea_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edFolderWork_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function edFolderWork_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edBusyBar_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function edBusyBar_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editSizeSuper_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function editSizeSuper_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listNumberOfROIs.
function listNumberOfROIs_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function listNumberOfROIs_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function editPixelNum_Callback(hObject, eventdata, handles)

handles.ScaleNum = str2double(get(handles.editPixelNum,'String'));
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function editPixelNum_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edTMIref_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edTMIref_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edFSAref_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function edFSAref_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edACIAref_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function edACIAref_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edArearef_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function edArearef_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slHema2_Callback(hObject, eventdata, handles)
common_HemaDAB_call(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function slHema2_CreateFcn(hObject, eventdata, handles)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slDAB2_Callback(hObject, eventdata, handles)
common_HemaDAB_call(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function slDAB2_CreateFcn(hObject, eventdata, handles)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function edHemaValue2_Callback(hObject, eventdata, handles)

a =  str2double(get(hObject,'String'));
set(handles.slHema2, 'Value', a);
common_HemaDAB_call(hObject, eventdata, handles);

guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edHemaValue2_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edDABValue2_Callback(hObject, eventdata, handles)

a =  str2double(get(hObject,'String'));
set(handles.slDAB2, 'Value', a);
common_HemaDAB_call(hObject, eventdata, handles);

guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edDABValue2_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slBin2_Callback(hObject, eventdata, handles)
common_Binary_call(hObject, eventdata, handles); %WORKS


% --- Executes during object creation, after setting all properties.
function slBin2_CreateFcn(hObject, eventdata, handles)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edBinValue2_Callback(hObject, eventdata, handles)

a =  str2double(get(hObject,'String'));
set(handles.slBin2, 'Value', a);

handles = common_Binary_call(hObject, eventdata, handles);
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edBinValue2_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btn_Fix_Binary2.
function btn_Fix_Binary2_Callback(hObject, eventdata, handles)

set(handles.pushLoadFile, 'BackgroundColor', [240/255 240/255 240/255]);

if strcmp(get(handles.edBinValue2, 'Enable'), 'off')
    set(handles.edBinValue2,'Enable', 'on', 'BackgroundColor', [240/255 240/255 240/255]);
    set(handles.btn_Fix_Binary2, 'BackgroundColor', [240/255 240/255 240/255], 'String', 'Fix');
    
    set(handles.slBin2,'Enable', 'on');
else
    set(handles.edBinValue2,'Enable', 'off', 'BackgroundColor', [1 215/255 174/255]);
    set(handles.btn_Fix_Binary2, 'BackgroundColor', [1 215/255 174/255], 'String', 'Fixed');
    set(handles.slBin2,'Enable', 'off');
end


% --- Executes on button press in btn_Fix_Hema2.
function btn_Fix_Hema2_Callback(hObject, eventdata, handles)

if strcmp(get(handles.edHemaValue2, 'Enable'), 'off')
    set(handles.edHemaValue2,'Enable', 'on', 'BackgroundColor', [240/255 240/255 240/255]);
    set(handles.btn_Fix_Hema2, 'BackgroundColor', [240/255 240/255 240/255], 'String', 'Fix');
    
    set(handles.slHema2,'Enable', 'on');
else
    set(handles.edHemaValue2,'Enable', 'off', 'BackgroundColor', [1 215/255 174/255]);
    set(handles.btn_Fix_Hema2, 'BackgroundColor', [1 215/255 174/255], 'String', 'Fixed');
    set(handles.slHema2,'Enable', 'off');
end


handles.hema2_in     = get(handles.slHema2,'Value');
set(handles.edHemaValue2,'String',handles.hema2_in);
handles = common_HemaDAB_call(hObject, eventdata, handles);
guidata(hObject,handles);

% --- Executes on button press in btn_Fix_DAB2.
function btn_Fix_DAB2_Callback(hObject, eventdata, handles)

if strcmp(get(handles.edDABValue2, 'Enable'), 'off')
    set(handles.edDABValue2,'Enable', 'on', 'BackgroundColor', [240/255 240/255 240/255]);
    set(handles.btn_Fix_DAB2, 'BackgroundColor', [240/255 240/255 240/255], 'String', 'Fix');
    
    set(handles.slDAB2,'Enable', 'on');
else
    set(handles.edDABValue2,'Enable', 'off', 'BackgroundColor', [1 215/255 174/255]);
    set(handles.btn_Fix_DAB2, 'BackgroundColor', [1 215/255 174/255], 'String', 'Fixed');
    set(handles.slDAB2,'Enable', 'off');
end

handles.DAB2_in      = get(handles.slDAB2,'Value');
set(handles.edDABValue2,'String',handles.DAB2_in);
handles = common_HemaDAB_call(hObject, eventdata, handles);
guidata(hObject,handles);


% --- Executes on button press in SavingResultsActivated.
function SavingResultsActivated_Callback(hObject, eventdata, handles)


function edAreaInter_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edAreaInter_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function lbFileReference_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function lbFileReference_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushSave.
function handles = pushSave_Callback(hObject, eventdata, handles)


% ---------------------------------------------------------------------
set(handles.edBusyBar,'BackgroundColor',[0 1 0],'String', 'saving ...' );
pause(0.001);
% ---------------------------------------------------------------------

 handles.SavingMode = 1;
 handles = common_HemaDAB_call(hObject, eventdata, handles);
 handles.SavingMode = 0;
guidata(hObject,handles);





function edAreaDAB_Callback(hObject, eventdata, handles)
% hObject    handle to edAreaDAB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edAreaDAB as text
%        str2double(get(hObject,'String')) returns contents of edAreaDAB as a double


% --- Executes during object creation, after setting all properties.
function edAreaDAB_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edAreaDAB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edAreaDABref_Callback(hObject, eventdata, handles)
% hObject    handle to edAreaDABref (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edAreaDABref as text
%        str2double(get(hObject,'String')) returns contents of edAreaDABref as a double


% --- Executes during object creation, after setting all properties.
function edAreaDABref_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edAreaDABref (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edPercentDAB_Callback(hObject, eventdata, handles)
% hObject    handle to edPercentDAB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edPercentDAB as text
%        str2double(get(hObject,'String')) returns contents of edPercentDAB as a double


% --- Executes during object creation, after setting all properties.
function edPercentDAB_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edPercentDAB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edPercentDABref_Callback(hObject, eventdata, handles)
% hObject    handle to edPercentDABref (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edPercentDABref as text
%        str2double(get(hObject,'String')) returns contents of edPercentDABref as a double


% --- Executes during object creation, after setting all properties.
function edPercentDABref_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edPercentDABref (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edAreaTotal_Callback(hObject, eventdata, handles)
% hObject    handle to edAreaTotal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edAreaTotal as text
%        str2double(get(hObject,'String')) returns contents of edAreaTotal as a double


% --- Executes during object creation, after setting all properties.
function edAreaTotal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edAreaTotal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
